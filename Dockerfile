FROM gcr.io/distroless/base-debian10

WORKDIR /app

COPY j2mao /app/

USER nonroot:nonroot

ENTRYPOINT ["/app/j2mao"]
