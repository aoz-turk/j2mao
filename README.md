**J2MAO**

This repo contains a sample Golang code, a sample CI pipeline and Kubernetes YAML files.

**CI pipeline**

.gitlab-ci.yml consists of  3 steps: test, build and push. All the steps are using Gitlab Docker executors.


1. test: Test function is being run with "go test" command. "go fmt" and "go vet" commands can also be added to this stage if needed.

2. build: The code is being compiled into an executable file.

3. push: A Docker image is being built and pushed to Gitlab container registry of this project. A distroless image is being used in FROM instruction to make the image lighter and nonroot user is being specified to run the container with a user that does not have root privileges.


**Kubernetes Deployment**

There are 2 files in k8s folder.

1. deployment.yaml: This file deploys 1 replica of j2mao pod to a Kubernetes cluster. We assume that there is a Redis instance running in redis-ns namespace which is exposed by a service named redis in the same Kubernetes cluster.

      If the image is stored in a private registry, "imagePullSecrets" parameter should be added to this yaml file or the default service account of the namespace. For some reason if a different service account is being used for the deployment, that service account can have "imagePullSecrets" definition as well.

2. service.yaml: This file exposes j2mao pod to outside world. If the Kubernetes cluster has support for service type LoadBalancer, an automatic IP address will be allocated and the service will be reachable from that IP. Also an ingress definiton can be used to expose the application, if there is an ingress controller in the Kubernetes cluster.


**References**

https://docs.gitlab.com/runner/executors/docker.html

https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
